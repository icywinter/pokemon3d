﻿var cannonball : Transform;
var cam : Camera;

function Update() {
var v : Vector3 = cam.ScreenToWorldPoint(Vector3(Input.mousePosition.x,Input.mousePosition.y,cam.nearClipPlane));
    if(Input.GetButtonUp("Fire1")) {
        var projectile = Instantiate(cannonball,Vector3(
                                     transform.position.x, v.position.y, transform.position.z),
                                     transform.rotation);
        projectile.rigidbody.AddForce(transform.forward * 100);//cannon's x axis
        Physics.IgnoreCollision(projectile.collider, collider);
    }
}